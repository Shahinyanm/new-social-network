<?php 
session_start();


if (!isset($_SESSION['user'])){

	header('location:index.php');
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Social</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" type="text/css" href="setings.css">

</head>
<body>
	
	<div class="navbar-fixed">
		<ul id="dropdown1" class="dropdown-content">
			<li><a href="#!">Settings</a></li>
			<li><a href="#!">Exit</a></li>
			<li class="divider"></li>
			<!--   <li><a href="#!">three</a></li> -->
		</ul>
		<nav>
			<div class="nav-wrapper">
				<a href="profile.php" class="brand-logo">New Social</a>
				<ul class="right hide-on-med-and-down">
					<li><a href="#">History</a></li>
					<li><a href="#">Contact</a></li>	
					<li id="request_in"><a href="#"><span class="glyphicon glyphicon-user"></span> Requests <sub id="num"> </sub></a> </li>      
					<li id="reg"><a href="#"> <input type="text" id="search" placeholder="Search" style="background:black; color:white;">  </button></a>
						<!-- Dropdown Trigger --><div id="s_req" > </div>
						<div id="req_in" style="display: none"> </div> </li>
						<li><a class="dropdown-button" href="#!" data-activates="dropdown1">Dropdown<i class="material-icons right">arrow_drop_down</i></a></li>
					</ul>
				</div>
			</nav>
		</div>



		<div id="container">
			<input type="text" hidden value="$_GET['id']" >
			<div class="row">
				<div id="contentsetings" class="col s12">

					<div id="leftbar" class="col s5"> 

						<div class="row">
							<form class="col s12" >
								<div class="row">
									<div class="input-field col s6">
										<input placeholder="Placeholder" id="first_name" type="text" class="validate" disabled>
										<label for="first_name">First Name</label>
									</div>
									<div class="input-field col s6">
										<input id="last_name" type="text" class="validate" disabled>
										<label for="last_name">Last Name</label>
									</div>
								</div>

								<div class="row">
									<div class="input-field col s12">
										<input id="password" type="password" class="validate" disabled>
										<label for="password">Password</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s12">
										<input id="email" type="email" class="validate" disabled readonly>
										<label for="email">Email</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s4">
										<input id="age" type="number" class="validate" disabled  >
										<label for="age">Age</label>
									</div>

									<div class=" col s4">
										<p>

											<input name="group1" type="radio" id="test1" disabled/>
											<label for="test1">male</label>
										</p>
										<p>
											<input name="group1" type="radio" id="test2" disabled/>
											<label for="test2">Formale</label>
										</p>
									</div>
									<div class="input-field col s4">
										<input id="country" type="text" class="validate" disabled>
										<label for="country">Country</label>
									</div>
								</div>
						
							</form>
							 <div class="switch">
    <label>
      Off
      <input type="checkbox" id="check">
      <span class="lever"></span>
      On
    </label>
  </div>

  
  
						</div>


					</div> 

					



<div id="rightbar" class="col s7"> 
				<div class="row">
					<div class="col s12">
					<div class="col s6">

					<form id="form" enctype="multipart/form-data">
						<div>
							<input type="file" id="img" multiple accept="image/*" name="img" style="display: none" />
						</div>
						<div>
							<img id="img-preview" src="uploads/images.png" />
							<br />
							<a href="#" id="reset-img-preview">удалить изображения</a>
						</div>
						<div>
							<input type="reset" value="Отмена"/>
							<button id="upload"/> Upload </div>
						</div>
					</form>
				</div>







			<div class="col s6" id="box">  
				<div class="slider" >
					<ul id="slide">

					 
						

					</ul>
</div>
				</div>
</div>
			</div>
		</div> 
	</div>

</div>





			
		</div>
		<div id="footer" class="col-md-6"> 

			<div id="footbox1" class="col-s-4">

				<p align="right"> Contact:  </p>
				<p align="right"> E-mail: </p>
				<p align="right"> <a href="#">About Us </a> </p>

				<p align="right"> © 2017–-> </p>




			</div>
			<div id="footbox2" class="col-s-4">

				<p> +374(98)88-26-82  </p>
				<p> shahinyanm@gmail.com</p>


			</div>



		</div>	


	</body>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

	<script src="setings.js"></script>
	</html>