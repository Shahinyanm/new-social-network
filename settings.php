<?php 
		session_start();


	if (!$_SESSION['user']){

		header('location', 'index.php');
}

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Social</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="header">
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php">New Social</a>
				</div>
				<ul class="nav navbar-nav">
					<li class="active"><a href="">Home</a></li>
					<li><a href="#">History</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li id="reg"><a href="#"> <input type="text" id="search" placeholder="Search" style="background:black; color:white;"> <button id="btnsearch" class="btn btn-warning"> <span class="glyphicon glyphicon-search"></span></button></a>
						<div id="s_req" > </div>
						<div id="req_in" style="display: none"> </div>
					</li>
					<li id="request_in"><a href="#"><span class="glyphicon glyphicon-user"></span> Requests <sub id="num"> 1</sub></a> </li>
					<li id="reg"><a href="settings.php"><span class="glyphicon glyphicon-user"></span> Settings</a></li>
					<li id="log"><a href="index.php"><span class="glyphicon glyphicon-log-out"></span> Exit</a></li>
				</ul>

			</div>
		</nav>
	</div>

	<div id="container">
		<input type="text" hidden value="$_GET['id']" >
		<div id="content">
		<div id="left" class="col-md-6"> 
		
		</div> 

		<div id="center" class="col-md-6">  

			<div class="form-group">
  				<br>
			<form action="server.php" method="post" id="my_form" enctype="multipart/form-data">
				<input type="file" name="upload" id="upload">
  				<label for="comment">Add your post:</label>
 				<textarea class="form-control" rows="2" id="post"></textarea>
 				<input type="color" name="color" id="col" id="col">
 				<input type="number" name="size" value="12" id="size">
 				<input type="submit" class="btn btn-primary" id="submit" value="Отправить">
			</form>
				
			</div>


		

		</div>

		<div id="right" class="col-md-6">
			
		</div>

		
		</div> 
	</div>
<div id="footer" class="col-md-6"> 

	<div id="footbox1" class="col-s-4">

		<p align="right"> Contact:  </p>
		<p align="right"> E-mail: </p>
		<p align="right"> <a href="#">About Us </a> </p>

		<p align="right"> © 2017–-> </p>




		</div>
	<div id="footbox2" class="col-s-4">
		
		<p> +374(98)88-26-82  </p>
		<p> shahinyanm@gmail.com</p>

		
	</div>



</div>	


</body>

<script src="profile.js"></script>
</html>