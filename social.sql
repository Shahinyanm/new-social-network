/*
Navicat MySQL Data Transfer

Source Server         : profitdc
Source Server Version : 100125
Source Host           : localhost:3306
Source Database       : social

Target Server Type    : MYSQL
Target Server Version : 100125
File Encoding         : 65001

Date: 2017-12-19 19:54:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `post_id` int(11) NOT NULL,
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `c_u` (`post_id`),
  CONSTRAINT `c_u` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comments
-- ----------------------------
INSERT INTO `comments` VALUES ('19', 'asdsadad', '29', '2017-12-16 10:19:53');
INSERT INTO `comments` VALUES ('20', 'asdsadsad', '29', '2017-12-16 10:36:17');

-- ----------------------------
-- Table structure for friends
-- ----------------------------
DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `r_my` (`user_id`),
  KEY `r_friend` (`friend_id`),
  CONSTRAINT `friends_ibfk_1` FOREIGN KEY (`friend_id`) REFERENCES `users` (`id`),
  CONSTRAINT `friends_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of friends
-- ----------------------------
INSERT INTO `friends` VALUES ('32', '37', '39');
INSERT INTO `friends` VALUES ('33', '38', '39');
INSERT INTO `friends` VALUES ('34', '39', '41');
INSERT INTO `friends` VALUES ('35', '39', '40');

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `status` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `i_u` (`user_id`),
  CONSTRAINT `i_u` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of images
-- ----------------------------
INSERT INTO `images` VALUES ('1', '39', 'images7.png', '1');
INSERT INTO `images` VALUES ('2', '39', 'images5.png', '0');

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `resiver_id` int(11) NOT NULL,
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `m_s` (`sender_id`),
  KEY `m_r` (`resiver_id`),
  CONSTRAINT `m_r` FOREIGN KEY (`resiver_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `m_s` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES ('14', 'asdadsadsad', '39', '41', '2017-12-19 16:51:34');
INSERT INTO `messages` VALUES ('15', 'asdasdsadas', '39', '40', '2017-12-19 16:51:48');
INSERT INTO `messages` VALUES ('16', 'dfdsfsdf', '39', '40', '2017-12-19 16:52:16');

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `color` varchar(20) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `p_u` (`user_id`),
  CONSTRAINT `p_u` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('29', 'asdasdad', '39', '#800080', '40', 'Koala.jpg');

-- ----------------------------
-- Table structure for requests
-- ----------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `r_my` (`user_id`),
  KEY `r_friend` (`friend_id`),
  CONSTRAINT `r_friend` FOREIGN KEY (`friend_id`) REFERENCES `users` (`id`),
  CONSTRAINT `r_my` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of requests
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('37', 'Mher', 'Mher', 'Mher@gmail.com', 'Mher', '10', 'dsfsf');
INSERT INTO `users` VALUES ('38', 'Mher', 'Shahinyan', 'shahinyanm@gmail.com', '$2y$10$JtwVe.4SoU6pRzMF7o0APO79ou.oPySHqq6vKE.xP4I1OWrAOg.cW', '23', 'hhh');
INSERT INTO `users` VALUES ('39', 'Mherik', 'Shahinyan', 's-mher@inbox.ru', '$2y$10$bfw2W.mZq8remebrI6h2fuxTyIyXEOA6bS9pUbVZfmOQBZQCCDH8i', '27', 'Hadrut');
INSERT INTO `users` VALUES ('40', 'Peto', 'Petrosyan', 'petojan@gmail.com', '$2y$10$rfW3HwU9JTlmPt2UnpIq6ehmlvPlQFl5LmfAESWWA5FmJG/iDPVjW', '28', 'erevan');
INSERT INTO `users` VALUES ('41', 'vahe', 'hovhannisyan', 'vahe@mail.ru', '$2y$10$11tv2//oyKkdP83q7oaotO0UwoGynI8by5xZvzD3Ijo/kavKdyPqK', '32', 'Yerevan');
INSERT INTO `users` VALUES ('47', 'zsdasdsa', 'sadada', 'asdsadad', 'adasdad', '19', 'asdad');

-- ----------------------------
-- Procedure structure for check_age
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_age`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `check_age`(x int(11))
BEGIN
DECLARE t int(11);
SET t = (SELECT age from users where id = x);
if t<18  then 
DELETE from users where id = x;
end if;




end
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for check_request
-- ----------------------------
DROP PROCEDURE IF EXISTS `check_request`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `check_request`( x int(11),y int (11))
BEGIN
DECLARE t int(11);
SET t = (SELECT count(*) from friends  where user_id = x and friend_id = y or user_id = y and friend_id = x  );
if t>0  then 
DELETE from requests where user_id = x and friend_id = y or  user_id = y and friend_id = x ;
end if;




end
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `triger_check_request`;
DELIMITER ;;
CREATE TRIGGER `triger_check_request` AFTER INSERT ON `friends` FOR EACH ROW begin
	call check_request (new.user_id, new.friend_id);







end
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `t1`;
DELIMITER ;;
CREATE TRIGGER `t1` AFTER INSERT ON `users` FOR EACH ROW begin
	call cheack_age (new.id);







end
;;
DELIMITER ;
