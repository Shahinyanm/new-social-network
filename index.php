<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Social</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href=" style.css">
</head>
<body>
	<?php  include_once 'header.php'; ?>

	<div id="container">
		<div id="content">
		<div id="box1" class="col-md-6"> 
			<p id="pcon"> We are connecting people </p>
			<img id="icon"  class="img-rounded" src="https://ibgme.ae/wp-content/uploads/2017/01/ea04d5728f7c721c89e6f12059c6c5f2_one-world-one-market-world-connections-clipart-png_1600-1200.png">

		</div> 

		<div id="box2" class="col-md-6 ">

			<div id="box3" class="row">
				<form class="col s12">
				<h2> LOGIN</h2>		
				
				 <div class="row">
				    <div class="input-field col s12">

				     	 <input id="emaillog" type="text" class="validate inp">
				     	 <label for="emaillog">E-mail</label>
				     	 <div id="err_email" style="display: none"> </div>
				  	</div>
				  </div>
				  <div class="row">
				    <div class="input-field col s12">
				     	<input id="passwordlog" type="password" class="validate inp">
				     	<label for="passwordlog">Password</label>
				     	<div id="err_pass" style="display: none"> </div>
				    </div>
    			 </div>
						

					<div class="input-field" id="butbox">
<!-- 						<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
 -->						<button id="btnlog"  class=" inp  btn waves-effect waves-light" > LogIn 
							<!-- <i class="material-icons right">send</i> -->
						</button>
					</div>
				</form>
		</div>
				


		<div id="box4" class="col-md-6">
			<form class="col s12">
			<h3> REGISTRATION FORM</h3>	
				
				<div class="row">
					<div class="input-field col s12">
						<!-- <input id="name" type="text" class=" inp  validate" name="name" placeholder="Write your name"> -->
						<input id="name" type="text" class="validate inp">
						<label for="passwordlog">Name</label>
						<div id="name" style="display: none"> </div>
					</div> 
				</div>



				<div class="row">
					<div class="input-field col s12">

						<input id="surname" type="text" class="validate inp">
						<label for="surname">Surname</label>
						<div id="err_regsurname" style="display: none"> </div>
					</div> 
				</div>
				<div class="row">
					<div class="input-field col s12">
						
						<input id="email" type="text" class="validate inp">
						<label for="email">E-mail</label>
						<div id="err_regemail" style="display: none"> </div>
					</div> 
				</div>
				<div class="row">
					<div class="input-field col s12" id="inpbox">
						
						<input id="password" type="password" class="validate inp">
						<label for="password">Password</label>
						<div id="err_regpass" style="display: none"> </div>
					</div> 
				</div>
				<div class="row">
					<div class="input-field col s12">							
						<input id="age" type="number" class="validate inp">
						<label for="age">Age</label>
						<div id="err_regage" style="display: none"> </div>
					</div> 
				</div>
				<div class="row">
					<div class="input-field col s12">					
						<input id="country" type="text" class="validate inp">
						<label for="country">Country</label>
						<div id="err_regcountry" style="display: none"> </div>
					</div> 
				</div>

					<div class="input-field" id="butbox">
						<button id="btnreg"  class=" inp  btn waves-effect waves-light" > Registration 
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
<br>
<br>
<br>
<br>
<br>
	<hr>
<div id="footer" class="col-md-6"> 

	<div id="footbox1" class="col-s-4">

		<p align="right"> Contact:  </p>
		<p align="right"> E-mail: </p>
		<p align="right"> <a href="#">About Us </a> </p>

		<p align="right"> © 2017–-> </p>




		</div>
	<div id="footbox2" class="col-s-4">
		
		<p> +374(98)88-26-82  </p>
		<p> shahinyanm@gmail.com</p>

		
	</div>



</div>	


</body>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

<script src="script.js"></script>
</html>