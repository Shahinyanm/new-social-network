<?php 
session_start();
include_once "models.php";
// $m = new Model();

// $d=$m->findAll();
// $m->insert(['name'=>'Mher', 'email'=>'Mher@gmail.com','age'=>'25']);
// $m->delete(['name'=>'Mher','id'=>'26']);
// $m->update('27',['name'=>'Mher','age'=>'27']);
// $r=$m->find(['name'=>'Mher','age'=>'27'], ['name','email',"id"]);

class app{

private $conn;

	function __construct(){

		if($_SERVER['REQUEST_METHOD']=='POST'){
			// echo json_encode($_POST);
			if(isset($_POST['action'])){

				$this->conn = new Model();
				$action = $_POST['action'];
				call_user_func([$this,$action]);
			}
		}

	}	

	function reg(){
		
		$name 		= $_POST['name'];
		$email 		= $_POST['email'];
		$surname 	= $_POST['surname'];
		$password 	= $_POST['password'];
		$age 		= $_POST['age'];
		$country 	= $_POST['country'];
		$table 		= $_POST['dtable'];
		$base 		= $_POST['dbasa'];
		
		$errors = [];
			$this->conn->select_database("$base");
			$this->conn->set_table("$table");
			
		if(!filter_var($email,FILTER_VALIDATE_EMAIL)){

			$errors[]= "erorr_email";
		}			
		
	
		if(!($this->conn->dupl_check($email))){

			$errors[]= "krknvox";
}

		if(empty($name)){

			$errors[]= "emp_name";
		}

		if(empty($surname)){

			$errors[]= "emp_surname";
		}

		if(empty($password)){

			$errors[]= "emp_password";
		}

		if(empty($country)){

			$errors[]= "emp_country";
		}

		if(!is_numeric($age)){

				$errors[]= "err_age";

		}
	


		// stugenq jisht e te che

		if(empty($errors)){

			$data = [
				'name' 		=> $name,
				'surname' 	=> $surname,
				'email' 	=> $email,
				'country' 	=> $country,
				'age' 		=> $age,
				'password' 	=> password_hash($password,PASSWORD_BCRYPT)
			];
			// $this->conn->select_database("$base");
			// $this->conn->set_table("$table");
			$this->conn->insert($data);
		}else {
			echo json_encode( $errors);
		}
	}
		
	function login(){

		$email 		= $_POST['email'];
		$password 	= $_POST['password'];
		$table 		= $_POST['dtable'];
		$base 		= $_POST['dbasa'];


		$data = [
				'email'		 => $email,
				'password' 	 => $password,
			];

		$this->conn->select_database("$base");
		$this->conn->set_table("$table");
		$r=$this->conn->find($data);
		foreach ($r as $key =>$value) {
			if($key=='password'){
				if(password_verify($_POST['password'], $value['password'] )){
					echo 1; 	
					$_SESSION['user'] = $value;
				}
			}
		}
	}


	function search(){

		$table 			= $_POST['dtable'];
		$base 			= $_POST['dbasa'];
		$search			= [];
		$search['name'] = $_POST['search'];

		
		$this->conn->select_database("$base");
		$this->conn->set_table("$table");		
		$r=$this->conn->find($search);
		echo json_encode($r);

	}

	function person(){
		
		$id						= $_POST['id'];

		
		$this->conn->select_database("social");

		$this->conn->set_table("users");		
		$r=$this->conn->find(['id' => $id]);

		$this->conn->set_table("requests");
		$h=$this->conn->find(['friend_id' => $id, 'user_id'=> $_SESSION['user']['id']]);

		$this->conn->set_table("friends");
		$f=$this->conn->find(['friend_id' => $id, 'user_id'=> $_SESSION['user']['id']]);
		
		if($h){

			$r[0]['status'] = 1;

		}elseif($f){

			$r[0]['status'] = 2;

		}else{

			$r[0]['status'] = 0;

		}


		echo json_encode($r);
	}


	function friend(){

		$userId					= $_SESSION['user']['id'];
		$friendId 				= $_POST['friend_id'];

		$this->conn->select_database("social");
		$this->conn->set_table("requests");		
		$this->conn->insert(['user_id'=>$userId,'friend_id'=>$friendId]);
		echo 1;


	}


	function check_request(){


		$this->conn->select_database("social");
		$this->conn->set_table("requests");
		$h=$this->conn->check_request(['user_id'=> $_SESSION['user']['id']]);

		echo json_encode ($h);
	}

	
	function set_friend(){

		$id=$_POST['id'];
		$this->conn->select_database("social");
		$this->conn->set_table("friends");
		$this->conn->insert(['user_id'=> $_SESSION['user']['id'], 'friend_id'=>$id]);

		// $this->conn->set_table("requests");
		// $this->conn->delete(['friend_id'=> $_SESSION['user']['id'], 'user_id'=>$id]);
	
		
	}

	function del_request(){

		$id=$_POST['id'];
		$this->conn->select_database("social");
		$this->conn->set_table("requests");
		$this->conn->delete(['friend_id'=> $_SESSION['user']['id'], 'user_id'=>$id]);
	
		
	}


	function set_post(){

 
		$data = array();
 
		    $error = false;
		    $files = array();
		 
		    $uploaddir = './uploads/';
		 
		 	// var_dump($_FILES['upload']);
		if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );
		 
		    // foreach( $_FILES as $file ){
		        if( move_uploaded_file( $_FILES['upload']['tmp_name'], $uploaddir . basename($_FILES['upload']['name']) ) ){
		            $files[] = realpath( $uploaddir . $_FILES['upload']['name'] );
		     	  }
		        else{
		            $error = true;
		        }
		    // }

		    $data = $error ? array('error' => 'Ошибка загрузки файлов.') : array('files' => $files );
		    

		    $img 	= 	$_FILES['upload']['name'];
		    $color	=	$_POST['color'];
		    $size	=	$_POST['size'];
		    $name 	=	$_POST['post'];

		    if(empty($name)){

		    	$data['error_textarea'] = $name;

		    }else{
		    
		    	if($error){echo json_encode( $data );}

		    $this->conn->select_database("social");
			$this->conn->set_table("posts");
			$this->conn->insert(['name'=> $name,'user_id'=> $_SESSION['user']['id'],'color'=>$color, 'size'=> $size, 'img'=>$img]);
			}
		}


		function check_friends() {


		    $this->conn->select_database("social");
			$this->conn->set_table("friends");
			$r = $this->conn->check_friend(['user_id'=>$_SESSION['user']['id']]);
			echo json_encode($r);
		}

		

		function post(){

			$this->conn->select_database("social");
			$this->conn->set_table("posts");
			$r = $this->conn->find(['user_id'=>$_SESSION['user']['id']]);
			// $r[]
			echo json_encode($r);
		}


		function comment_post(){

			$this->conn->select_database("social");
			$this->conn->set_table("comments");
			$this->conn->insert(['name'=>$_POST['text'],'post_id'=>$_POST['id'],'time'=>date("Y-m-d H:i:s")]);
			// $r[]


		}

		function comments(){
			// echo $_POST['id'];
			$this->conn->select_database("social");
			$this->conn->set_table("comments");
			$r = $this->conn->find(['post_id'=>$_POST['id']]);
			echo json_encode($r);
		}

		function delete_post(){
			$this->conn->select_database("social");
			$this->conn->set_table("posts");
			$this->conn->delete(['id'=>$_POST['id']]);


		}

		function delete_comment(){
			$this->conn->select_database("social");
			$this->conn->set_table("comments");
			$this->conn->delete(['id'=>$_POST['id']]);


		}

		function check_setings(){
			$this->conn->select_database("social");
			$this->conn->set_table("users");
			$r=$this->conn->find(['id'=>$_SESSION['user']['id']]);
			echo json_encode($r);
		}

		function save(){

			$this->conn->select_database("social");
			$this->conn->set_table("users");
			$this->conn->update($_SESSION['user']['id'],['name'=>$_POST['name'],'surname'=>$_POST['surname'],'age'=>$_POST['age'],'country'=>$_POST['country']]);

		}

		function set_image(){

			$data = array();
 
		    $error = false;
		    $files = array();
		 
		    $uploaddir = './upload/';
		 
		 	// var_dump($_FILES['upload']);
		if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );
		 
		    // foreach( $_FILES as $file ){
		        if( move_uploaded_file( $_FILES['img']['tmp_name'], $uploaddir . basename($_FILES['img']['name']) ) ){
		            $files[] = realpath( $uploaddir . $_FILES['img']['name'] );
		     	  }
		        else{
		            $error = true;
		        }
		    // }

		    $data = $error ? array('error' => 'Ошибка загрузки файлов.') : array('files' => $files );
		    	
		    	var_dump($data);

		    $img 	= 	$_FILES['img']['name'];


		 
		    $this->conn->select_database("social");
			$this->conn->set_table("images");
			$this->conn->insert(['user_id'=> $_SESSION['user']['id'],'img'=>$img]);

		}	



		function sliders(){

			$this->conn->select_database("social");
			$this->conn->set_table("images");
			$r = $this->conn->find(['user_id'=>$_SESSION['user']['id']]);
			echo json_encode($r);

		}


		function def(){

			$this->conn->select_database("social");
			$this->conn->set_table("images");
			$this->conn->update_images($_SESSION['user']['id'],['status'=>'0']);
			$this->conn->update($_POST['id'],['status'=>'1']);

		}

		function check_image(){

			$this->conn->select_database("social");
			$this->conn->set_table("images");
			$r = $this->conn->find(['user_id'=>$_SESSION['user']['id'], 'status'=>'1']);
			echo json_encode($r);
		}

		function profile_image(){

			$this->conn->select_database("social");
			$this->conn->set_table("images");
			$r = $this->conn->find(['user_id'=>$_SESSION['user']['id'],'status'=>'1']);

			$this->conn->set_table("users");
			$h = $this->conn->find(['id'=>$_SESSION['user']['id']]);


			$arr = array_merge($r, $h);
			// var_dump($arr);
			echo json_encode($arr);

		}


		function sendMessage(){

			$this->conn->select_database("social");
			$this->conn->set_table("messages");
		$this->conn->insert(['sender_id'=> $_SESSION['user']['id'],'resiver_id'=>$_POST['id'], 'message'=>$_POST['text'],'time'=>date("Y-m-d H:i:s")]);
		

		}


		function friendMessages(){

			$this->conn->select_database("social");
			$this->conn->set_table("messages");
			$r = $this->conn->check_message(['sender_id'=>$_SESSION['user']['id'],'resiver_id'=>$_POST['id']]);
			echo json_encode($r);
		}	

	}
	


$app = new app();
 ?>